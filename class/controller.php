<?php
	class controller{

		private 	$db;

		public 		$view;
		public 		$model;
		public 		$conf;
		protected	$library;

		public function __construct(){
			$this->conf		= new configuration();
			$this->view 	= new view();
			$this->model 	= new model();
			$this->db		= new database();
			$this->library	= new library();
			if(!isset($_SESSION['user'])){
				$_SESSION['user'] = "prasetyayosef";
			}
		}
		
		public function id(){
			return $_SESSION["user"]. date("YmdHis");
		}
		public function notif($status, $msg){
			echo "<p class='notif ". $status. "' align='center'>". $msg. "</p>";	
		}
	
	}
