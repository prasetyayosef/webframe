<?php
	class configuration{

		private $conf;
		private $db;

		public $base_url;
		public $upl;
		
		public function __construct(){
			include(BASE_DIR. DS. "config.php");
			$this->db 	= $_WEB['DB'];
			$this->url	= $_WEB['BASE']['URL'];
			$this->upl	= $_WEB['DIR']['REPORT'];
		}
	
		/* 
			url formater
			this method used directly in view file
			parameter:
				$page = controller subclass
				$action = sub controller method (note: empty $action will be reference to method index)
			example:
				<a href="<?php echo $this->conf->url("class", "method"); ?>">file_view</a>
		*/
		public function url($page="", $action=""){
			$url = $this->url. "page=". $page;
			if(!empty($action))
				$url .= "&&action=". $action;
			return $url;
		}
		
		/*
			mysqli instance
			this method used for connecting mysqli API
		*/
		public function mysqli_instance(){
			$sqli = @new mysqli($this->db['HOST'], $this->db['USER'], $this->db['PASS'], $this->db['NAME']);
			if($sqli->connect_error) throw new Exception("_CONF_ERROR: Database not connected!");
			return $sqli;
		}

		/* 
			file upload formater
			this method used for upload any file into server upload directory
			parameter:
				$up_name = HTTP POST variable name
			example:
				$this->conf->upload("image"); note: "image" is similar with $_FILES["image"]["tmp_name"]
		*/
		public function upload($up_name){
			$up_file 	= $this->upl. $_FILES[$up_name]['name'];
			if(move_uploaded_file($_FILES[$up_name]['tmp_name'], $up_file)){
				return 1;
			}else throw new Exception("_CONF_ERROR: File not uploaded!");
		}

	}
