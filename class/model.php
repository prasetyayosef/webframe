<?php
	class model{

		public $query;

		public function __construct(){
			$this->query = new database();
		}

		public function load($class){
			$loc_file = BASE_DIR. DS. "app". DS. "model". DS. $class. ".php";
			if(file_exists($loc_file)){
				require_once($loc_file);
				$this->$class = new $class();
			}else throw new Exception("_MODEL_ERROR: model $class didnt exists!");
		}

		public function sql($sql){
			return $this->query->sql($sql);
		}

		public function getAll($param = ""){
			return $this->query->getAll($this->table, $param);
		}

		public function getAllWhere($where, $params = ""){
			return $this->query->getAllWhere($this->table, $where, $params);
		}

		public function getWhere($col, $where, $param = ""){
			return $this->query->getWhere($this->table, $col, $where, $param);
		}

		public function getAllJoinWhere($join, $where, $param = ""){
			return $this->query->getAllJoinWhere($this->table, $join, $where, $param);
		}

		public function getJoinWhere($col, $join, $where, $param = ""){
			/*
				Example:
					$col 	= array(table1, table2, tablen)
					$join 	= array(table2 => array(PK.table1 => FK.table2), 
									tablen => array(PK.tablen => FK.tablen));
					$where 	= array(col1 => value1, coln => valuen);
					$param 	= BETWEEN blablabla
			*/
			return $this->query->getJoinWhere($this->table, $col, $join, $where, $param);
		}

		public function insert($data){
			return $this->query->insert($this->table, $data);
		}

		public function update($data, $where, $param = ""){
			/*
				Example:
					$data = array(col1 => value1, coln => valuen)
					$where = array(col2 => value2)
			*/
			return $this->query->update($this->table, $data, $where, $param);
		}

		public function updateJoin($join, $data, $where, $param = ""){
			return $this->query->updateJoin($this->table, $join, $data, $where, $param);
		}

		public function delete($where){
			return $this->query->delete($this->table, $where);
		}
	}

