<?php
	class database{

		public $set;
		private $sql;
		private $query;

		public function __construct(){
			$this->set = new configuration();
		}

		public function sql($sql){
			$this->sql = $sql;
			return $this->exec();
		}

		/*
			Query Method
			This method used for sql query formater
		*/


		public function getAll($table, $param = ""){
			$this->sql = "SELECT *FROM ". $table;
			if(!empty($param)) $this->sql .= $param;
			return $this->exec();
		}

		public function getAllWhere($table, $where = [], $param = ""){
			$this->sql = "SELECT *FROM ". $table. " WHERE ";
			$i = 0;
			foreach($where as $key => $value){
				$i++;
				$this->sql .= $key. " = '". $value. "'";
				if($i < count($where)) $this->sql .= " AND ";
			}
			if(!empty($param)) $this->sql .= $param;
			return $this->exec();
		}

		public function getWhere($table, $col, $where = [], $param = ""){
			$this->sql = "SELECT ";
			if(is_array($col)){
				$i = 0;
				foreach($col as $value){
					$i++;
					$this->sql .= $value;
					if($i < count($col)) $this->sql .= ", ";
				}
			}else $this->sql .= $col;
			$this->sql .= " FROM ". $table. " WHERE ";
			$j = 0;
			foreach($where as $key => $value){
				$j++;
				$this->sql .= $key. " = '". $value. "'";
				if($j < count($where)) $this->sql .= " AND ";
			}
			if(!empty($param)) $this->sql .= $param;
			return $this->exec();
		}

		public function getAllJoinWhere($table, $join = [], $where = [], $param = ""){
			$this->sql = "SELECT *FROM ". $table;
			foreach($join as $key => $value){
				$this->sql .= " INNER JOIN ". $key. " ON ";
				$i = 0;
				foreach($value as $k => $v){
					$i++;
					$this->sql .= $k. " = ". $v;
					if($i < count($value)) $this->sql .= ", ";
				}
			}
			$this->sql .= " WHERE ";
			$j = 0;
			foreach($where as $key => $value){
				$j++;
				$this->sql .= $key. " = ". "'$value'";
				if($j < count($where)) $this->sql .= " AND ";
			}
			if(!empty($param)) $this->sql .= " ". $param;
			return $this->exec();
		}

		public function getJoinWhere($table, $col, $join = [], $where = [], $param = ""){
			$this->sql = "SELECT ";
			if(is_array($col)){
				$i = 0;
				foreach($col as $value){
					$i++;
					$this->sql .= $value;
					if($i < count($col)) $this->sql .= ", ";
				}
			}else $this->sql .= $col;
			$this->sql .= " FROM ". $table;
			foreach($join as $key => $value){
				$this->sql .= " INNER JOIN ". $key. " ON ";
				$l = 0;
				foreach($value as $k => $v){
					$l++;
					$this->sql .= $k. " = ". $v;
					if($l < count($value)) $this->sql .= ", ";
				}
			}
			$this->sql .= " WHERE ";
			$j = 0;
			foreach($where as $key => $value){
				$j++;
				$this->sql .= $key. " = ". "'$value'";
				if($j < count($where)) $this->sql .= " AND ";
			}
			if(!empty($param)) $this->sql .= " ". $param;
			return $this->exec();
		}
		
		public function insert($table, $data = []){
			$this->sql = "INSERT INTO ". $table. " (";
			$i = 0;
			foreach($data as $key => $value){
				$i++;
				$this->sql .= $key;
				if($i < count($data)) $this->sql .= ",";
			}
			$this->sql .= ") VALUES (";
			$j = 0;
			foreach($data as $value){
				$j++;
				$this->sql .= "'$value'";
				if($j < count($data)) $this->sql .= ", ";
			}
			$this->sql .= ")";
			return $this->exec();
		}

		public function update($table, $data = [], $where = [], $param = ""){
			$this->sql = "UPDATE ". $table. " SET ";
			$i = 0;
			foreach($data as $key => $value){
				$i++;
				$this->sql .= $key. " = '". $value. "'";
				if($i < count($data)) $this->sql .= ", ";
			}
			$this->sql .= " WHERE ";
			$j = 0;
			foreach($where as $key => $value){
				$j++;
				$this->sql .= $key. " = '". $value. "'";
				if($j < count($where)) $this->sql .= " AND ";
			}
			if(!empty($param)) $this->sql .= $param;
			return $this->exec();
		}

		public function updateJoin($table, $join = [], $data = [], $where = [], $param = ""){
			$this->sql = "UPDATE ". $table;
			foreach($join as $key => $value){
				$this->sql .= " INNER JOIN ". $key. " ON ";
				$i = 0;
				foreach($value as $k => $v){
					$i++;
					$this->sql .= $k. " = ". $v;
					if($i < count($value)) $this->sql .= ", ";
				}
			}
			$this->sql .= " SET ";
			$j = 0;
			foreach($data as $key => $value){
				$j++;
				$this->sql .= $key. " = '". $value. "'";
				if($j < count($data)) $this->sql .= ", ";
			}
			$this->sql .= " WHERE ";
			$k = 0;
			foreach($where as $key => $value){
				$k++;
				$this->sql .= $key. " = '". $value. "'";
				if($k < count($where)) $this->sql .= " AND ";
			}
			if(!empty($param)) $this->sql .= $param;
			return $this->exec();
		}
		
		public function delete($table, $where = []){
			$this->sql = "DELETE FROM ". $table. " WHERE ";
			foreach($where as $key => $value){
				$this->sql .= $key. " = ". "'$value'";
			}
			return $this->exec();
		}
		/*
			Query Execution Method
			This method used for creating executable query
		*/
		public function exec(){
			$this->query = mysqli_query($this->set->mysqli_instance(), $this->sql);
		}

		public function to_array(){
			$data = [];
			while($res = mysqli_fetch_array($this->query)){
				array_push($data, $res);
			}
			return $data;
		}

		public function num_rows(){
			return mysqli_num_rows($this->query);
		}

	}

