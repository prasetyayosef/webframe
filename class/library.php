<?php
	class library{

		public function __construct(){

		}

		public function load($lib, $par = false){
			$loc_file = BASE_DIR. DS. "app". DS. "library". DS. $lib. DS. $lib. ".php";
			if(file_exists($loc_file)){
				require_once($loc_file);
				if($par == false){
					$this->$lib = new $lib($par);
				}
			}else throw new Exception("_LIB_ERROR: library '$lib' didnt exists!");
		}
	}

